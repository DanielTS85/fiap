import time

numero_digitado  = int(input("Digite o número desejado: "))

if ((numero_digitado == 0) or (numero_digitado == 1)):
    print("Ação bem sucedida!")
else:
    sequencia = 0
    numero_anterior = 1
    numero_posterior = 1
    while (sequencia < numero_digitado):
        sequencia = numero_anterior + numero_posterior
        numero_posterior = numero_anterior
        numero_anterior = sequencia
    if (sequencia == numero_digitado):
        print("Ação bem sucedida!")
    else:
        print("A ação falhou...") 

time.sleep(5)