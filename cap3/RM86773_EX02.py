import time

qtd_transacoes  = int(input("Quantos transações você realizou hoje? "))
total_transacoes  = 0.00

for x in range (1, (qtd_transacoes+1)):
    total_transacoes += float(input("Informe o valor da transação {}: R$ ". format(x)))

media_transacoes = total_transacoes / qtd_transacoes

print("Você gastou R$ {:.2f} no total de hoje. A média de transação foi de R$ {:.2f}". format(total_transacoes, media_transacoes))

time.sleep(5)