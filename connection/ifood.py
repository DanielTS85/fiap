import time

places = ["Kibon Sorveteria - Saúde", "Sukia - Saúde", "A Feijoada", "Makis Place", "Giraffas Carrefour Metrocar", "Viena - Shopping Santa Cruz"]
ratings = [4.9, 4.6, 4.4, 4.7, 4.4, 4.4]
shippings = [6.99, 7.99, 9.90, 7.99, 5.99, 12.49]

ratings_sort = False

while ratings_sort == False:
    ratings_sort = True
    for i in range(len(ratings) - 1):
        if (ratings[i] <= ratings[i + 1]):
            
            temp_rating_max = ratings[i]
            temp_rating_min = ratings[i + 1]

            temp_place_max = places[i]
            temp_place_min = places[i + 1]

            temp_shipping_max = shippings[i]
            temp_shipping_min = shippings[i + 1]
            
            if ((ratings[i] == ratings[i + 1]) and (shippings[i] > shippings[i + 1])) or (ratings[i] < ratings[i + 1]):
                ratings_sort = False
                
                ratings[i] = temp_rating_min
                ratings[i+1] = temp_rating_max

                places[i] = temp_place_min
                places[i+1] = temp_place_max

                shippings[i] = temp_shipping_min
                shippings[i+1] = temp_shipping_max

#Display
for i in range(len(ratings)):
    print('Estabelecimento: {}  | Avaliação: {}  | Frete: R$ {}'. format(places[i], ratings[i], shippings[i]))

time.sleep(5)