import time

plano       = input("Digite o plano contratado: ")
faturamento = float(input("Digite o faturamento anual do plano: "))

if plano.upper() == 'BASIC':
    bonus = faturamento * 0.30
elif plano.upper() == 'SILVER':
    bonus = faturamento * 0.20
elif plano.upper() == 'GOLD':
    bonus = faturamento * 0.10
elif plano.upper() == 'PLATINUM':
    bonus = faturamento * 0.05
else:
    bonus = 0
    
if (bonus == 0):
    print("Plano não identificado")
else:
    print("O valor do bônus a ser pago é: {}" . format(bonus))

time.sleep(5)