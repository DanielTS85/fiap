import time

peso    = float(input("Insira seu PESO: "))
altura  = float(input("Insira sua ALTURA: "))
imc     = peso / (altura ** 2)

if imc < 16.00:
    print("Sua faixa de IMC é: Baixo peso Grau III")
elif imc >= 16.00 and imc <= 16.99:
    print("Sua faixa de IMC é: Baixo peso Grau II")
elif imc >= 17.00 and imc <= 18.49:
    print("Sua faixa de IMC é: Baixo peso Grau I")
elif imc >= 18.50 and imc <= 24.99:
    print("Sua faixa de IMC é: Peso ideal")
elif imc >= 25.00 and imc <= 29.99:
    print("Sua faixa de IMC é: Sobrepeso")
elif imc >= 30.00 and imc <= 34.99:
    print("Sua faixa de IMC é: Obesidade Grau I")
elif imc >= 35.00 and imc <= 39.99:
    print("Sua faixa de IMC é: Obesidade Grau II")
else:
    print("Sua faixa de IMC é: Obesidade Grau III")

time.sleep(5)